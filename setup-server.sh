#!/usr/bin/env bash

declare _PROGNAME

_PROGNAME=$(basename -- "$0")


_DOCKER() {
        echo -e "Installing Docker Community Edition..."
        sudo apt -y install apt-transport-https ca-certificates curl software-properties-common
        curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
        sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
        sudo apt update
        sudo apt -y install docker-ce docker-compose
        sudo usermod -aG docker $USER
}

_DROPBEAR() {
        echo -e "Installing Dropbear..."
        sudo apt -y install dropbear
        sudo systemctl disable dropbear
        sudo bash -c 'cat > /etc/dropbear-initramfs/authorized_keys' <<EOF
# Authoized keyfile goes here
EOF

        echo -e "Do you want to set a static IP for Dropbear?"
        while true; do
                read -p "Select (y/n)?" choice
                case "$choice" in
                y|Y ) sudo bash -c 'cat >> /etc/initramfs-tools/initramfs.conf' <<EOF
IP=10.0.0.99::10.0.0.1:255.255.255.0::eno1:off
EOF
                break ;;
                n|N ) echo -e "Your DHCP server will give Dropbear a unique address \n" ; break ;;
                  * ) echo -e "invalid \n" ;;
                esac
        done

        wget https://codeberg.org/noflcl/scripts/raw/branch/main/crypt_unlock.sh
        sudo mv crypt_unlock.sh /etc/initramfs-tools/hooks/
        sudo chmod +x /etc/initramfs-tools/hooks/crypt_unlock.sh

        sudo bash -c 'cat >> /etc/dropbear-initramfs/config' <<EOF
DROPBEAR_OPTIONS="-p 2244"
EOF

        echo -e "\n Updating initramfs...\n"

        sudo update-initramfs -u
        }

_IOMMU() {
        # Always run last, reboot required
        echo -e "\nEnabling iommu groups and isolating your GPU... \n"
        # "amd_iommu=on iommu=pt kvm_amd.npt=1 kvm_amd.avic=1"
        #sudo perl -i -pe 's/(GRUB_CMDLINE_LINUX_DEFAULT=.*)"/\1 amd_iommu=on iommu=pt"/' /etc/default/grub
    sudo perl -i -pe 's/(GRUB_CMDLINE_LINUX_DEFAULT=.*)"/$1amd_iommu=on iommu=pt vfio-pci.ids=1002:67df,1002:aaf0"/' /etc/default/grub

                echo -e "Does the format of your grub file look correct? \n"
                grep GRUB_CMDLINE_LINUX_DEFAULT /etc/default/grub
        while true; do
                read -p "Select (y/n)?" choice
                case "$choice" in
                y|Y ) echo -e "Your system will reboot now..." ; break ;;
                n|N ) echo -e "Please manually edit your /etc/default/grub file before rebooting your system. \n"; exit 1 ;;
                  * ) echo -e "invalid \n" ;;
                esac
        done

        echo -e "\nYou can now reboot your server. \n"
}

_KVM() {
        sudo apt -y install qemu-kvm qemu-utils ovmf libvirt-daemon-system libvirt-clients bridge-utils virt-manager
        sudo usermod -aG kvm "${USER}"
        sudo usermod -aG libvirt "${USER}"

        echo -e "\nDo you want to install a minimal desktop for x2go sessions? \n"
                while true; do
                read -p "Select (y/n)?" choice
                case "$choice" in
                y|Y ) sudo apt -y install --no-install-recommends xorg
                sudo apt -y install openbox lxpolkit
                sudo apt -y install --no-install-recommends x2goserver x2goserver-xsession
                bash -c cat > '$HOME/.xinitrc' <<EOF
#!/bin/bash
exec openbox-session
EOF
                if [ ! -d ~/.config ]; then
                mkdir ~/.config
                fi

                cp -R /etc/xdg/openbox ~/.config/
                bash -c cat > '$HOME/.config/autostart.sh' <<EOF
lxpolkit &
EOF
                chmod +x ~/.config/autostart.sh ; break ;;
                n|N ) echo -e "No desktop session has been installed. \n"; break ;;
                  * ) echo -e "invalid \n" ;;
                esac
        done
                # If using VS code on the remote host uncomment this:
                #sudo bash -c cat >> '/etc/x2go/x2goagent.options' <<EOF
#X2GO_NXAGENT_DEFAULT_OPTIONS+=" -extension BIG-REQUESTS"
#EOF
}

_UFW() {
        sudo ufw allow from 10.0.0.15/32 to any port 22
        # Samba rule gets set during it's configuration
        sudo ufw enable
}

_UPDATE() {
        echo -e "Checking for updates..."
        sudo apt update
        sudo apt -y upgrade
}

_WIREGUARD() {
        # Enable packet forwarding
        sudo bash -c 'cat > /etc/sysctl.d/99-wireguard.conf' <<EOF
net.ipv4.ip_forward=1
net.ipv6.conf.all.forwarding=1
EOF
        sudo sysctl -p /etc/sysctl.d/99-wireguard.conf
        sudo apt -y install wireguard
}

_ZFS() {
        # Install ZFS and import all pools
        sudo apt -y install zfsutils-linux
        sudo zpool import -a -f

        if zpool list >/dev/null; then
                echo -e "\n   Your zpool has imported correctly\n"
        else
                echo -e "\n   Your zpool did not import correctly.\n"
                echo -e "   Check your ZFS logs."
        fi
}

while [ ! $# -eq 0 ]; do
        case "$1" in
        --all | -a)
                _UPDATE
                _UFW
                _DROPBEAR
                _DOCKER
                _KVM
                _WIREGUARD
                _ZFS
                _IOMMU
                ;;
        --update | -u)
                _UPDATE
                ;;
        --ufw | -f)
                _UFW
                ;;
        --dropbear | -d)
                _DROPBEAR
                ;;
        --docker | -c)
                _DOCKER
                ;;
        --kvm | -k)
                _KVM
                ;;
        --wire | -w)
                _WIREGUARD
                ;;
        --zfs | -z)
                _ZFS
                ;;
        --iommu | -i)
                _IOMMU
                ;;
        esac
        shift
done

usage() {
        cat <<EOF

Usage: $_PROGNAME <options>
Options:
    -a, --all           Install complete list
    -u, --update        Update the system
    -f, --ufw           Configure and enable UFW firewall
    -d, --dropbear      Install and configure dropbear
    -c, --docker        Install and configure docker
    -k, --kvm           Install and configure kvm and qemu
    -w, --wire          Install and configure wireguard
    -z, --zfs           Install zfs and import all available pools
    -i, --iommu         Install and configure iommu groups (requires reboot)

EOF
}

if [[ "$1" == "" ]]; then
        usage
        exit 0
fi

exit 0