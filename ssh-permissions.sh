#!/usr/bin/env bash

###
# Set correct permssions for SSH
###

HOME="/home/$USER/"

chmod 700 $HOME/.ssh
chmod 644 $HOME/.ssh/authorized_keys
chmod 644 $HOME/.ssh/config
chmod 644 $HOME/.ssh/known_hosts
chmod 644 $HOME/.ssh/*.pub

# Find a universal way to add the private keypairs in any given SSH directory
declare -a KEYS=("id_rsa" "unlock")

for i in "${KEYS[@]}"
do
        chmod 600 $HOME/.ssh/"$i"
done

echo -e "\nDone setting permissions\n"