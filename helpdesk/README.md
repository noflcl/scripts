![console output from the helpdesk script](assets/img/console.webp)

# HelpDesk ![wizard hat logo](helpdesk.png)

A basic script that connects users systems to a Tailscale `tailnet` so I can offer remote support.

### Requirements

`tailscale` - This can be easily adapted to Wireguard commands (thats where it started).

### Setup

Place the icon `.png` in `/home/$USER/.local/share/icons/`

Place the `.desktop` file in`/home/$USER/.local/share/applications/`

Place the `.sh` script in `/home/$USER/.local/bin/`

### ToDo

- [ ] Make more verbose so user has an idea of whats going on
- [ ] Write a simple setup script
