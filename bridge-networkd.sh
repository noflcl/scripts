#!/usr/bin/env bash

###
# Bridged ethernet for KVM - VMs can access lan now
###

sudo bash -c 'cat > /etc/systemd/network/enp8s0f1.network' <<EOF
[Match]
Name=enp8s0f1

[Link]
RequiredForOnline=no

[Network]
ConfigureWithoutCarrier=true
Address=10.0.0.105/16
EOF

sudo systemctl restart systemd-networkd.service

sudo ip link add name bridge_kvm type bridge
sudo ip link set bridge_kvm up
sudo ip link set enp8s0f1 up
sudo ip link set enp8s0f1 master bridge_kvm


sudo bash -c 'cat > /etc/systemd/network/mykvmBridge.netdev' <<EOF
[NetDev]
Name=br0
Kind=bridge
EOF

sudo systemctl restart systemd-networkd.service

sudo bash -c 'cat > /etc/systemd/network/bindBridge.network' <<EOF
[Match]
Name=enp8s0f1

[Network]
Bridge=br0
EOF

sudo bash -c 'cat > /etc/systemd/network/localBridge.network' <<EOF
[Match]
Name=br0

[Network]
DHCP=ipv4
EOF

sudo systemctl restart systemd-networkd.service