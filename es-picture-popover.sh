#display a picture:

eog --fullscreen /home/pi/Pictures/splash.png&
sleep 0.5

#make sure emulationstation does not pop over our eog picture window:
wmctrl -r eog -b add,above
sleep 0.5

#start emulation station
emulationstation --gamelist-only --no-splash&

#once es is done loading stop displaying the picture

sleep 5
pkill eog

#give input control back to es
wmctrl -a EmulationStation 