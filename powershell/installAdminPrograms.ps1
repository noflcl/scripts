    #Install New apps
    $apps = @(
        @{name = "Guru3D.Afterburner" },
        @{name = "smartmontools.smartmontools" }, 
        @{name = "WiresharkFoundation.Wireshark" }, 
        @{name = "GlassWire GlassWire.GlassWire" }, 
        @{name = "Telerik.Fiddler.Everywhere" }, 
        # Sysinternals tool suite
        @{name = "sysinternals"; source = "msstore" }, 
        # Yubico Authenticator
        @{name = "9NFNG39387K0"; source = "msstore" }, 
        @{name = "REALiX.HWiNFO" }
    );
    Foreach ($app in $apps) {
        #check if the app is already installed
        $listApp = winget list --exact -q $app.name
        if (![String]::Join("", $listApp).Contains($app.name)) {
            Write-host "Installing:" $app.name
            if ($app.source -ne $null) {
                winget install --exact --silent $app.name --source $app.source --accept-package-agreements
            }
            else {
                winget install --exact --silent $app.name --accept-package-agreements 
            }
        }
        else {
            Write-host "Skipping Install of " $app.name
        }
    }