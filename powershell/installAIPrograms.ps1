    #Install New apps
    $apps = @(
        # Luminar Neo - Photo Editor
        @{name = "9P7JQGL6GC8P"; source = "msstore" },
        # AI toast and chat trainer
        @{name = "XP9C8B2VJTCH5N"; source = "msstore" },
        # Wondershare Filmora - AI Video Editor
        @{name = "XP8JNPKGTV6NRL"; source = "msstore" }
    );
    Foreach ($app in $apps) {
        #check if the app is already installed
        $listApp = winget list --exact -q $app.name
        if (![String]::Join("", $listApp).Contains($app.name)) {
            Write-host "Installing:" $app.name
            if ($app.source -ne $null) {
                winget install --exact --silent $app.name --source $app.source --accept-package-agreements
            }
            else {
                winget install --exact --silent $app.name --accept-package-agreements 
            }
        }
        else {
            Write-host "Skipping Install of " $app.name
        }
    }