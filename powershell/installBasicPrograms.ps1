    #Install New apps
    $apps = @(
        @{name = "9NZVDKPMR9RD"; source = "msstore" },
        @{name = "Giorgiotani.Peazip" }, 
        @{name = "Valve.Steam" },
        # Foxit pdf viewer 
        @{name = "XPFCG5NRKXQPKT"; source = "msstore" },
        # yt-dlg 
        @{name = "XP9CCFSWS911F5"; source = "msstore" },
        # VLC
        @{name = "XPDM1ZW6815MQM"; source = "msstore" },
        # GIMP
        @{name = "XPDM27W10192Q0"; source = "msstore" }
    );
    Foreach ($app in $apps) {
        #check if the app is already installed
        $listApp = winget list --exact -q $app.name
        if (![String]::Join("", $listApp).Contains($app.name)) {
            Write-host "Installing:" $app.name
            if ($app.source -ne $null) {
                winget install --exact --silent $app.name --source $app.source --accept-package-agreements
            }
            else {
                winget install --exact --silent $app.name --accept-package-agreements 
            }
        }
        else {
            Write-host "Skipping Install of " $app.name
        }
    }