    #Install New apps
    $apps = @(
        @{name = "Microsoft.dotnet" },
        @{name = "Microsoft.PowerShell" }, 
        # @{name = "Microsoft.VisualStudioCode" }, 
        # vscode from msstore
        @{name = "XP9KHM4BK9FZ7Q"; source = "msstore" },
        @{name = "Microsoft.WindowsTerminal"; source = "msstore" }, 
        @{name = "Microsoft.PowerToys" }, 
        @{name = "Docker.DockerDesktop" },
        @{name = "Git.Git" }, 
        @{name = "BlueStack.BlueStacks" }, 
        @{name = "GitHub.cli" }
    );
    Foreach ($app in $apps) {
        #check if the app is already installed
        $listApp = winget list --exact -q $app.name
        if (![String]::Join("", $listApp).Contains($app.name)) {
            Write-host "Installing:" $app.name
            if ($app.source -ne $null) {
                winget install --exact --silent $app.name --source $app.source --accept-package-agreements
            }
            else {
                winget install --exact --silent $app.name --accept-package-agreements 
            }
        }
        else {
            Write-host "Skipping Install of " $app.name
        }
    }